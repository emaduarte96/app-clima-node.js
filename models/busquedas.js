const fs = require('fs');
const axios = require('axios');

class Busquedas {
    historial = []
    dbPath = './db/database.json';

    constructor(){
        this.leerDB();
    }

    get paramMapBox(){
        return{
            params:{   
                access_token: process.env.MAPBOX_KEY,
                limit: 6, 
                language: 'es',
            }
        }
    }

    get paramOpenWeather(){
        return{
                appid: process.env.OPENWEATHER_KEY,
                units: 'metric',
                lang:'es',
            }
    }

    get historialCapitalizado(){
        return this.historial.map(lugar =>{

            let palabras = lugar.split(' ');
            palabras = palabras.map(p => p[0].toUpperCase() + p.substring(1));

            return palabras.join(' ');
        })
    }

    


    async ciudad(lugar = ''){

       try{
        
        const urlBase = `https://api.mapbox.com/geocoding/v5/mapbox.places/${lugar}.json`;
           
        //peticion HTTP

        const resp = await axios.get(urlBase, this.paramMapBox);
        
        return resp.data.features.map( lugar => ({
            id: lugar.id,
            nombre: lugar.place_name,
            lng: lugar.center[0],
            lat: lugar.center[1],
        }));

    

       }catch(e){
        console.log(e);
        return[]

       }

    }

    async climaDelLugar(lat='', lon=''){
        
        try{

            const instance = axios.create({
                baseURL: 'https://api.openweathermap.org/data/2.5/weather',
                params: {...this.paramOpenWeather, lat, lon} 
            });

            const resp = await instance.get();

            const {main, weather} = resp.data;

            return{
                temp: main.temp,
                max:  main.temp_max,
                min:  main.temp_min,
                desc: weather[0].description,
            }

        }catch(e){
            console.log(e);
        }
    }

    agregarHistorial(lugar=''){
        
        if(this.historial.includes(lugar.toLocaleLowerCase())){
            return;
        }
        this.historial.unshift(lugar);

        this.guardarDB();

    }


    guardarDB(){
        const payload = {
            historial: this.historial,
        }

        fs.writeFileSync(this.dbPath, JSON.stringify(payload));
    }

    leerDB(){
        if(!fs.existsSync(this.dbPath)) return;

        const info = fs.readFileSync(this.dbPath,  {encoding:'utf8'});
        
        const data = JSON.parse(info);
        
        this.historial = data.historial;
    }
}


module.exports = Busquedas;