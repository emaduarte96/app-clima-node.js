const inquirer = require('inquirer');
require ('colors');

const preguntas = [
    {   
        type: 'list',
        name: 'opcion',
        message: `${'¿Qué desea hacer?'.red}`,
        choices: [
            
            {
                value: 1,
                name: `${'1.'.red} Buscar un lugar`,
            },
            {
                value: 2,
                name: `${'2.'.red} Historial`,
            },
            {
                value: 0,
                name: `${'0.'.red} Salir`,
            },
            
        ]
    },

];



const inquirerMenu = async() => {
    
    console.log('==============================='.america);
    console.log('     Seleccione una opción');
    console.log('===============================\n'.america);
    
    const {opcion} = await inquirer.prompt(preguntas);

    return opcion;
}


const pausa = async() =>{

    const question = [
        {
            type: 'input',
            name: 'enter',
            message: `\nPresione ${'ENTER'.green} para continuar\n`,
        }
    ]
    
    await inquirer.prompt(question); 
}

const leerInput = async(message) => {

    const question = [
        {
            type: 'input',
            name: 'desc',
            message,
            validate(value){
                if(value.length === 0){
                    return 'Por favor ingrese un valor';
                }
                return true;
            }
        }
    ] 

    const {desc} = await inquirer.prompt(question)
    return desc;
}

const listarResultados = async(resultados) => {
    let choices = [];
    let cont = -1;

    resultados.forEach((resul, i) => {
        const {id, nombre, lng, lat} = resul;
        cont += 1;
        
        let data = {
            value: id,
            name: `${i}.`.blue + ' ' + nombre,
            long: lng,
            latitud: lat,
        }
        
        choices[cont] = data;
    
    });
        
    
    let lugaresListados = [
        {   
            type: 'list',
            name: 'id',
            message: `${'Seleccione lugar'.red}`,
            choices
            
    }];

    const {id} = await inquirer.prompt(lugaresListados)

    return id;
}



module.exports = {
    inquirerMenu,
    pausa,
    leerInput,
    listarResultados,
}