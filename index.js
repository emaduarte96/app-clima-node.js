const inquirer = require('inquirer');
const {leerInput, inquirerMenu, pausa, listarResultados} = require('./helpers/inquirer');
const Busquedas = require('./models/busquedas');
require('dotenv').config();


const main = async() =>{
    const busquedas = new Busquedas();
    let opt;
    
    do{

        opt = await inquirerMenu();
        
        switch(opt){
            case 1:
                //Mostrar mensaje
                const lugar = await leerInput('Ciudad:'.yellow);

                //Buscar lugar
                const resultados = await busquedas.ciudad(lugar);
                
                //Seleccionar lugar
                const idSeleccionado = await listarResultados(resultados);
                const lugarSeleccionado = resultados.find(l => l.id === idSeleccionado);

                //Grabar en DB
                busquedas.agregarHistorial(lugarSeleccionado.nombre);
                
                //Clima
                const clima = await busquedas.climaDelLugar(lugarSeleccionado.lat, lugarSeleccionado.lng);

                //Mostrar resultados
                console.log('\nInformacion de la ciudad\n'.green);
                console.log('Ciudad:', lugarSeleccionado.nombre);
                console.log('Lat:', lugarSeleccionado.lat);
                console.log('Lng:', lugarSeleccionado.lng);
                console.log('Temperatura:', clima.temp);
                console.log('Min:', clima.min )
                console.log('Max:', clima.max )
                console.log('Descripcion:', clima.desc)
                
                break;

            case 2:
                busquedas.historialCapitalizado.forEach((l,i) =>{
                    const idx = `${i + 1}.`.green
                    console.log(idx , l);
                });


                break;
        }

       

        if(opt !== 0) await pausa();

    }while(opt !== 0);

}

main();